package b137.abano.s03a1;
import java.math.BigInteger;
import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        System.out.println("Factorial\n");

        //create a java program that accepts an integer that computes for the factorial value and display it to the console.

        Scanner appScanner = new Scanner(System.in);
        System.out.println("Enter number:");
        BigInteger number, i, factorial = new BigInteger("1");
        number = appScanner.nextBigInteger();

        for(i = new BigInteger("1"); i.compareTo(number)<=0;i=i.add(BigInteger.ONE)) {
            factorial = factorial.multiply((i));
        }
        System.out.println("Factorial of " + number + " is "+ factorial);

    }
}
